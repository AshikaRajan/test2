import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {
  transform(tabledata: any[], searchtext: any): any[] {
    if (!tabledata) {
      return [];
    }
    if (!searchtext) {
      return tabledata;
    }
    searchtext = searchtext.toLowerCase();
    return tabledata.filter(it => {
     return it.username.toLowerCase().indexOf(searchtext.toLowerCase()) !== -1;
    });
  }
}
