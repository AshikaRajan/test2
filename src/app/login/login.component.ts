import { Component } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  comment: String = 'successfully logged in!!';
  loggedin: Boolean = false;
  signin = {
    username: '',
    password: '',
    logintime: ''
  };
  tabledata = [];
  constructor(private router: Router) {}
  login() {
    this.loggedin = true;
    setTimeout(function() {
        this.loggedin = false;
      }.bind(this), 2000);
    this.tabledata.push({ ...this.signin });
    this.signin = { username: '', password: '', logintime: '' };
  }
  changePage() {
    localStorage.setItem('userData', JSON.stringify(this.tabledata));
    this.router.navigate(['user-data']);
  }
}
