import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-userdata',
  templateUrl: './userdata.component.html',
  styleUrls: ['./userdata.component.css']
})
export class UserdataComponent implements OnInit {
    tabledata = [];
    today: number = Date.now();
    constructor(private router: Router) {
    }
    ngOnInit() {
      const data = JSON.parse(localStorage.getItem('userData'));
      this.tabledata = data;
    }
    logout() {
      this.router.navigate(['/login']);
      localStorage.clear();
    }
  }
