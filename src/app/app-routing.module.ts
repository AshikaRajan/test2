import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserdataComponent } from './userdata/userdata.component';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: '/login'},
  {
    path: 'login',
    component: LoginComponent
  },
  {path: 'user-data', component: UserdataComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
